//import './App.css';
import ReactDOM from "react-dom";
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import Login from './components/Login';
import { createBrowserHistory } from "history";
import {createStore} from 'redux'
import {Provider} from 'react-redux'
import reducer from "./redux/reducer";

const store=createStore(reducer)
const customHistory = createBrowserHistory();
function App() {
  return (
    <Provider store={store}>
    <Switch>
      <Route exact path="/" component={Login}  />
      {/* <Route exact path="/templates" component={Templates}/> */}
    </Switch>
    </Provider>  
  );
}

export default App;
