import React, { Component } from "react";
import ReactTable from "react-table";

export class Table extends Component {
  render() {
    return (
      <div>
        {/* <h1 style={{ paddingLeft: 30, flex: "0 0 30px" }}>Templates</h1> */}
        <div style={{ display: "flex", flexGrow: 1, flexFlow: "column" }}>
          <div>
            <ReactTable
              noDataText="We couldn't find anything"
              filterable={true}
              defaultPageSize={20}
              sortable={true}
              style={{ height: "85%", width: "95%", marginLeft: 30 }}
              columns={this.props.columns}
              data={this.props.templates}
              onClick={this.props.handleTableViewAuditClick}
              loading={this.props.loading ? this.props.loading : false}
              onFetchData={this.props.onFetchData}
              nextText={this.props.nextText}
              defaultFilterMethod={this.props.defaultFilterMethod}
              page={this.props.page}
              onPageChange={this.props.onPageChange}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default Table;
