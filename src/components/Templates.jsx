import React, { useState, useEffect } from "react";
import axios from "axios";
import "./Table.css";
import Login from "./Login";
//import Table from "./Table";
import { connect } from "react-redux";
import "./styles/style.css";
import ReactTable from "react-table";
import { Segment, Icon } from "semantic-ui-react";
import {
  Sidebar,
  Grid,
  Label,
  Button,
  Statistic,
  Modal,
  Menu,
  Dropdown,
} from "semantic-ui-react";
import logo from "./images/textLogo.png";
import logotwo from "./images/matrix-logo.png";

const Templates = (props) => {
  const [templateData, settemplateData] = useState([]);
  const [Error, setError] = useState("");
  const [apitoken, setApiToken] = useState("");
  const [sideBarVisible, setSideBarVisible] = useState(true);
  const [SelectedTemplate, setSelectedTemplate] = useState({});
  const [selectedTemplateData, setSelectedTemplateData] = useState("");
  const [thisTemplatesView, setThisTemplatesView] = useState(true);
  const [modalOpen, setModalOpen] = useState(false);
  const [template, setTemplate] = useState({});
  const [count, setCount] = useState("");
  const [firstDateChoose, setFirstDateChoose] = useState(null);

  // const templates = props.templateData;
  // export const setupAxios = (Token) => {
  //     axios.defaults.headers.common['Authorization'] = "Bearer " + Token;
  // }

  const getmethod = () => {
    console.log(props, "++++");
  };

  useEffect(() => {
    //   getmethod()
    console.log(props, "simplee");
    if (props.token) {
      setApiToken(props.token);
      axios
        .get(
          "https://apstaging.matrixbscloud.com/apis/" +
            "/auditor/v1/atm/getTemplate",
          {
            headers: {
              "Content-Type": "application/json",
              Authorization: `Bearer ${props.token}`,
            },
          }
        )
        .then((response) => {
          console.log(response, "receive");
          settemplateData(response.data);
        })
        .catch((error) => {
          setError(error.message);
        });
    }
  }, []);
  console.log(templateData, "PPP");

  const toggleSidebar = () => {
    setSideBarVisible(!sideBarVisible);
  };

  // handleCloseClick = () => {
  //   setThisTemplatesView(true);
  // };

  const handleTableViewAuditClick = (template) => {
    setSelectedTemplate(template);
    setThisTemplatesView(false);
  };

  // downloadReport = (template) => {
  //   downloadAtmReportAction(template._id);
  // };

  // hersheyscloseEditUser = () => {
  //   setModalOpen(false);
  // };

  const columns = [
    {
      Header: "Template Name",
      accessor: "",
      style: { textAlign: "center", cursor: "pointer" },
      Cell: row =>
          <AuditTableCell
            row={row.original}
            text={row.original.auditName}
            onClick={handleTableViewAuditClick}
          />
    },
    {
      Header: "Report",
      accessor: "ICON",
      width: 150,
      style: { textAlign: "center", cursor: "pointer" },
      Cell: row => {
            return (
              <Icon
                size="large"
                color="green"
                name="ICON"
              />
            );
          return <AuditTableCell row={row.original} text="" />;
        }
    },
  ];

  return (
    <div style={{ height: "100%" }}>
      <Segment raised style={{ backgroundColor: "#fafafa", height: 60 }}>
        <div style={{ display: "inline-block" }}>
          <Icon
            style={{
              display: "inline-block",
              cursor: "pointer",
              float: "left",
              color: "#606060",
              marginTop: 4,
            }}
            onClick={toggleSidebar}
            size="big"
            name="bars"
          />
          <img
            style={{
              height: 120,
              marginTop: -40,
              resize: "horizontal",
              float: "left",
            }}
            src={logo}
          />
          <img
            style={{
              height: 60,
              marginTop: -13,
              float: "left",
            }}
            src={logotwo}
          />
        </div>
      </Segment>
      <div>
        <h2 style={{ paddingLeft: 30, flex: "0 0 30px" }}>Templates</h2>
      </div>
      <div style={{ display: "flex", flexGrow: 1, flexFlow: "column" }}>
        <div>
          <ReactTable
            noDataText="We couldn't find anything"
            filterable={true}
            defaultPageSize={20}
            sortable={true}
            style={{ height: "85%", width: "95%", marginLeft: 30 }}
            columns={columns}
            onClick={handleTableViewAuditClick}
            data={templateData}
          />
        </div>
      </div>
    </div>
  );
};

function mapStateToProps(state) {
  return {
    Token: state.Token,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setToken: (value) => {
      dispatch({ type: "setToken", value });
    },
  };
}

function AuditTableCell(props) {
  function onClick() {
    props.onClick(props.row);
  }
  return (
    <div style={props.style} onClick={onClick}>
      {props.text}
    </div>
  );
}

// const mapStateToProps = (state) => {
//   return {
//     atm: state.atm,
//   };
// };

// const mapDispatchToProps = (dispatch) => {
//   return bindActionCreators(
//     {
//       fetchAtmTemplatesAction: fetchAtmTemplatesAction,
//       downloadAtmReportAction: downloadAtmReportAction,
//     },
//     dispatch
//   );
// };

export default connect(mapStateToProps, mapDispatchToProps, AuditTableCell)(Templates);
// export default Templates;
