import React, { useState } from "react";
import { Button, Form, FormGroup, Label, Input } from "reactstrap";
import Templates from "./Templates";

import "./Login.css";
import axios from "axios";
import { useHistory } from "react-router-dom";
import { connect } from "react-redux";

export const setupAxios = (Token) => {
  axios.defaults.headers.common["Authorization"] = "Bearer " + Token;
};

function Login(props) {
  const [loggedin, setLoggedIn] = useState(false);
  const [Email, setEmail] = useState("");
  const [Password, setPassword] = useState("");
  const [Error, setError] = useState("");
  const [Token, setToken] = useState("");
  const history = useHistory();

  console.log(history);

  const Submit = async () => {
    //console.log("getted");
    //alert("Stored..")
    await axios
      .post("https://apstaging.matrixbscloud.com/apis/" + "/auditor/v1/login", {
        email: Email,
        password: Password,
      })
      .then((response) => {
        // history.push("/templates");
        console.log(response, "received..");
        console.log(response.data);
        setToken(response.data.access_token);
        props.setToken(response.data.access_token);
        setLoggedIn(true);
      })
      .catch((error) => {
        setError(error.message);
        setLoggedIn(false);
      });
  };

  return (
    <div>
      {loggedin === false ? (
        <div className="login-form">
          <h1 className="text-center">Welcome</h1>
          <FormGroup>
            <Label>Email</Label>
            <Input
              onChange={(e, index) => {
                setEmail(e.target.value);
              }}
              type="email"
              placeholder="Email"
            />
          </FormGroup>
          <FormGroup>
            <Label>Password</Label>
            <Input
              onChange={(e, index) => {
                setPassword(e.target.value);
              }}
              type="password"
              placeholder="Password"
            />
          </FormGroup>
          <div style={{ display: "flex" }}>
            <Button onClick={Submit}>Log in</Button>
          </div>
        </div>
      ) : (
        <Templates token={Token} />
      )}
    </div>
  );
}

function mapStateToProps(state) {
  return {
    Token: state.Token,
  };
}

// const mapStateToProps = (state) => ({ Token: state.Token })

function mapDispatchToProps(dispatch) {
  return {
    setToken: (value) => dispatch({ type: "setToken", value }),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);
